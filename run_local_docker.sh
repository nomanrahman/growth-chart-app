#!/usr/bin/env bash

docker build --build-arg TARGET_ENV=local -t growth-chart-app/node-web-app .
docker run -p 9000:9000 growth-chart-app/node-web-app
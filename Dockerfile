FROM node:alpine

RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh
RUN git clone https://github.com/smart-on-fhir/growth-chart-app.git
WORKDIR /growth-chart-app
RUN npm install
ADD /images/logo.png /growth-chart-app/images/logo.png
ADD /images/hspc-company-logo.png /growth-chart-app/images/hspc-company-logo.png
ADD /src/.well-known/smart/manifest*.json /growth-chart-app/.well-known/smart/
RUN mv /growth-chart-app/index.html /growth-chart-app/app.html
ADD /src/index.html /growth-chart-app/index.html
ADD /src/css/app.css /growth-chart-app/css/app.css
RUN rm /growth-chart-app/launch.html
ADD /src/launch.html /growth-chart-app/launch.html
ARG TARGET_ENV
ENV TARGET_ENV=$TARGET_ENV
RUN if [ "$TARGET_ENV" = "prod" ]; then rm /growth-chart-app/.well-known/smart/manifest.json; mv /growth-chart-app/.well-known/smart/manifest.prod.json /growth-chart-app/.well-known/smart/manifest.json; elif [ "$TARGET_ENV" = "test" ]; then rm /growth-chart-app/.well-known/smart/manifest.json; mv /growth-chart-app/.well-known/smart/manifest.test.json /growth-chart-app/.well-known/smart/manifest.json; fi
CMD ["./node_modules/http-server/bin/http-server", "-p", "9000", "-c-1", ".", "--cors"]
